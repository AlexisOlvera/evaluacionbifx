<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class home
 * @package App\Models
 * @version October 18, 2018, 6:14 pm UTC
 *
 * @property string title
 * @property string description
 * @property string image
 */
class home extends Model
{
    public $table = 'home';

    public $timestamps = false;

    protected $primaryKey = 'id';

    public $fillable = [
        'title',
        'description',
        'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
