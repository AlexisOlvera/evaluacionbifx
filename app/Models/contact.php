<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class contact
 * @package App\Models
 * @version October 18, 2018, 8:41 pm UTC
 *
 * @property string name
 * @property string email
 * @property string subject
 * @property string description
 */
class contact extends Model
{
    public $table = 'contact';

    public $timestamps = false;

    protected $primaryKey = 'id';

    public $fillable = [
        'name',
        'email',
        'subject',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'subject' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
