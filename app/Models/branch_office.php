<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class branch_office
 * @package App\Models
 * @version October 18, 2018, 10:07 pm UTC
 *
 * @property string name
 * @property string address
 * @property float lat
 * @property float lng
 * @property string description
 */
class branch_office extends Model
{

    public $timestamps = false;

    public $table = 'branch_office';

    protected $primaryKey = 'id';

    public $fillable = [
        'name',
        'address',
        'lat',
        'lng',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'address' => 'string',
        'lat' => 'float',
        'lng' => 'float',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
