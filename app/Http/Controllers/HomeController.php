<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatehomeRequest;
use App\Http\Requests\UpdatehomeRequest;
use App\Repositories\homeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\Home;

class homeController extends AppBaseController
{
    /** @var  homeRepository */
    private $homeRepository;

    public function __construct(homeRepository $homeRepo)
    {
        $this->homeRepository = $homeRepo;
    }

    /**
     * Display a listing of the home.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->homeRepository->pushCriteria(new RequestCriteria($request));
        //$homes = $this->homeRepository->all();

        $homes = $this->homeRepository->findWithoutFail(1);

        return view('layouts.app')
            ->with('homes', $homes);
    }

    /**
     * Show the form for creating a new home.
     *
     * @return Response
     */
    public function create()
    {
        return view('homes.create');
    }

    /**
     * Store a newly created home in storage.
     *
     * @param CreatehomeRequest $request
     *
     * @return Response
     */
    public function store(CreatehomeRequest $request)
    {
        dd($request);
        $input = $request->all();

        $home = $this->homeRepository->create($input);

        Flash::success('Home saved successfully.');

        return redirect(route('homes.index'));
    }

    /**
     * Display the specified home.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $home = $this->homeRepository->findWithoutFail($id);

        if (empty($home)) {
            Flash::error('Home not found');

            return redirect(route('homes.index'));
        }

        return view('homes.show')->with('home', $home);
    }

    /**
     * Show the form for editing the specified home.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $home = $this->homeRepository->findWithoutFail($id);

        if (empty($home)) {
            Flash::error('Home not found');

            return redirect(route('homes.index'));
        }

        return view('homes.edit')->with('home', $home);
    }

    /**
     * Update the specified home in storage.
     *
     * @param  int              $id
     * @param UpdatehomeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatehomeRequest $request)
    {
        $home = $this->homeRepository->findWithoutFail($id);

        if (empty($home)) {
            return response()->json(['message' => 'Ocurrio un error al actualizar.'], 404);
        }

        Storage::delete('home/' . $home->image);

        try {
            $home->update([
                'title'=>$request->home['title'],
                'description'=>$request->home['description'],
                'image'=>'without_image',
            ]);

            $image = explode(',', $request->home['image']);
            $imageContent = base64_decode($image[1]);
            $imageName = $id.".".$request->home['imageFormat'];
            $imageRoute = 'home/' . $imageName;
            Storage::put($imageRoute, $imageContent);
            $home->image = $imageName;
            $home->save();

            DB::commit();
            return response()->json(['message' => 'Informacion actualizada correctamente.'], 200);
        }catch (QueryException $e) {
            DB::rollback();
            return response()->json(['message' => 'Ocurrió un error al actualizar: ' . $e->getMessage()], 422);
        }

        /*$home = $this->homeRepository->findWithoutFail($id);

        if (empty($home)) {
            Flash::error('Home not found');

            return redirect(route('homes.index'));
        }

        $home = $this->homeRepository->update($request->all(), $id);

        Flash::success('Home updated successfully.');

        return redirect(route('homes.index'));*/
    }

    /**
     * Remove the specified home from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $home = $this->homeRepository->findWithoutFail($id);

        if (empty($home)) {
            Flash::error('Home not found');

            return redirect(route('homes.index'));
        }

        $this->homeRepository->delete($id);

        Flash::success('Home deleted successfully.');

        return redirect(route('homes.index'));
    }
}
