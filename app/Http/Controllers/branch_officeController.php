<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createbranch_officeRequest;
use App\Http\Requests\Updatebranch_officeRequest;
use App\Repositories\branch_officeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\Branch_office;

class branch_officeController extends AppBaseController
{
    /** @var  branch_officeRepository */
    private $branchOfficeRepository;

    public function __construct(branch_officeRepository $branchOfficeRepo)
    {
        $this->branchOfficeRepository = $branchOfficeRepo;
    }

    /*
     * get brachoffices
     */
    public function getBranchOffice(){

        $branchOffice = Branch_office::all();

        return response()->json([
            'success' => 1,
            'data' => $branchOffice]);
    }

    /**
     * Display a listing of the branch_office.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->branchOfficeRepository->pushCriteria(new RequestCriteria($request));
        $branchOffices = $this->branchOfficeRepository->all();

        return view('branch_offices.index')
            ->with('branchOffices', $branchOffices);
    }

    /**
     * Show the form for creating a new branch_office.
     *
     * @return Response
     */
    public function create()
    {
        return view('branch_offices.create');
    }

    /**
     * Store a newly created branch_office in storage.
     *
     * @param Createbranch_officeRequest $request
     *
     * @return Response
     */
    public function store(Createbranch_officeRequest $request)
    {
        $input = $request->all();

        $branchOffice = $this->branchOfficeRepository->create($input);

        Flash::success('Branch Office saved successfully.');

        return redirect(route('branchOffices.index'));
    }

    /**
     * Display the specified branch_office.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $branchOffice = $this->branchOfficeRepository->findWithoutFail($id);

        if (empty($branchOffice)) {
            Flash::error('Branch Office not found');

            return redirect(route('branchOffices.index'));
        }

        return view('branch_offices.show')->with('branchOffice', $branchOffice);
    }

    /**
     * Show the form for editing the specified branch_office.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $branchOffice = $this->branchOfficeRepository->findWithoutFail($id);

        if (empty($branchOffice)) {
            Flash::error('Branch Office not found');

            return redirect(route('branchOffices.index'));
        }

        return view('branch_offices.edit')->with('branchOffice', $branchOffice);
    }

    /**
     * Update the specified branch_office in storage.
     *
     * @param  int              $id
     * @param Updatebranch_officeRequest $request
     *
     * @return Response
     */
    public function update($id, Updatebranch_officeRequest $request)
    {
        $branchOffice = $this->branchOfficeRepository->findWithoutFail($id);

        if (empty($branchOffice)) {
            Flash::error('Branch Office not found');

            return redirect(route('branchOffices.index'));
        }

        $branchOffice = $this->branchOfficeRepository->update($request->all(), $id);

        Flash::success('Branch Office updated successfully.');

        return redirect(route('branchOffices.index'));
    }

    /**
     * Remove the specified branch_office from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $branchOffice = $this->branchOfficeRepository->findWithoutFail($id);

        if (empty($branchOffice)) {
            Flash::error('Branch Office not found');

            return redirect(route('branchOffices.index'));
        }

        $this->branchOfficeRepository->delete($id);

        Flash::success('Branch Office deleted successfully.');

        return redirect(route('branchOffices.index'));
    }
}
