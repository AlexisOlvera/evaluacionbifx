<?php

namespace App\Repositories;

use App\Models\branch_office;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class branch_officeRepository
 * @package App\Repositories
 * @version October 18, 2018, 10:07 pm UTC
 *
 * @method branch_office findWithoutFail($id, $columns = ['*'])
 * @method branch_office find($id, $columns = ['*'])
 * @method branch_office first($columns = ['*'])
*/
class branch_officeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'address',
        'lat',
        'lng',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return branch_office::class;
    }
}
