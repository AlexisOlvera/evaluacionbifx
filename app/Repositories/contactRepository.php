<?php

namespace App\Repositories;

use App\Models\contact;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class contactRepository
 * @package App\Repositories
 * @version October 18, 2018, 8:41 pm UTC
 *
 * @method contact findWithoutFail($id, $columns = ['*'])
 * @method contact find($id, $columns = ['*'])
 * @method contact first($columns = ['*'])
*/
class contactRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
        'subject',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return contact::class;
    }
}
