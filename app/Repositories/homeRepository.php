<?php

namespace App\Repositories;

use App\Models\home;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class homeRepository
 * @package App\Repositories
 * @version October 18, 2018, 6:14 pm UTC
 *
 * @method home findWithoutFail($id, $columns = ['*'])
 * @method home find($id, $columns = ['*'])
 * @method home first($columns = ['*'])
*/
class homeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'description',
        'image'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return home::class;
    }
}
