<html>
<head></head>
<body style="background: white; color: black">
<h1>Nuevo mensaje con el asunto: <b style="color: #2a72b5; text-transform: uppercase">{{ $subject }}</b> de parte de: <b style="color: #2a72b5; text-transform: uppercase">{{ $name }}</b></h1>
<br><br>
<h2>Descripción</h2>
<br>
<p style="color: #2a72b5; text-transform: uppercase">{{ $description }}</p>
</body>
</html>