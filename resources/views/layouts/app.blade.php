<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Project</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    {!! Html::style('http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700') !!}
    {!! Html::style('assets/plugins/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('assets/plugins/font-awesome/css/font-awesome.min.css') !!}
    {!! Html::style('assets/css/animate.min.css') !!}
    {!! Html::style('assets/css/style.min.css') !!}
    {!! Html::style('assets/css/style-responsive.min.css') !!}
    {!! Html::style('assets/css/theme/default.css') !!}
    <!-- ================== END BASE CSS STYLE ================== -->

    <!-- ================== BEGIN BASE JS ================== -->
    <script async="" src="../../../../www.google-analytics.com/analytics.js"></script><script src="assets/plugins/pace/pace.min.js"></script>
    <!-- ================== END BASE JS ================== -->
    @yield('css')

    {!!Html::style("assets/css/styles.css")!!}

    <!-- ================== BEGIN PLUGINS ================== -->
    {!! Html::style('assets/plugins/parsley/src/parsley.css') !!}
    {!! Html::style('assets/plugins/kartik-v-bootstrap-fileinput/css/fileinput.css') !!}
    {!! Html::style('assets/plugins/bootstrap-toastr/toastr.min.css') !!}

    <!-- ================== END PLUGINS ================== -->
</head>


{{-- begin include´s --}}
@include('homes.modalHome')
{{-- end include´s --}}


<body data-spy="scroll" data-target="#header-navbar" data-offset="51" class="  pace-done">

    <div class="pace  pace-inactive hide">
        <div class="pace-progress" data-progress-text="100%" data-progress="99" style="width: 100%;">
            <div class="pace-progress-inner"></div>
        </div>
        <div class="pace-activity"></div>
    </div>

<!-- begin #page-container -->
<div id="page-container" class="fade in">
    <!-- begin #header -->
    <div id="header" class="header navbar navbar-transparent navbar-fixed-top">
        <!-- begin container -->
        <div class="container">
            <!-- begin navbar-header -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header-navbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="index-2.html" class="navbar-brand">
                    <span class="brand-logo"></span>
                    <span class="brand-text">
                        <span class="text-theme">TPC</span>
                    </span>
                </a>
            </div>
            <!-- end navbar-header -->
            <!-- begin navbar-collapse -->
            <div class="collapse navbar-collapse" id="header-navbar">
                <ul class="nav navbar-nav navbar-right">
                    <li class=""><a href="#home" data-click="scroll-to-target">INICIO</a></li>
                    <li class=""><a href="#contact" data-click="scroll-to-target">CONTACTO</a></li>
                    <li class=""><a href="#branch_office" data-click="scroll-to-target">SUCURSALES</a></li>
                    @if(!Auth::guest())
                    <li class="dropdown active">
                        <a {{--href="#home"--}} data-click="scroll-to-target" data-toggle="dropdown"> CONFIGURACION <b class="caret"></b></a>
                        <ul class="dropdown-menu dropdown-menu-left animated fadeInDown">
                            <li><a id="btnConfigHome">Inicio</a></li>
                            <li><a id="">Sucursales</a></li>
                        </ul>
                    </li>
                    @endif
                    <li class="dropdown active">
                        <a href="#home" data-click="scroll-to-target" data-toggle="dropdown">{!! Auth::user()->name !!} <b class="caret"></b></a>
                        <ul class="dropdown-menu dropdown-menu-left animated fadeInDown">
                            <li><a href="{!! url('/logout') !!}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    Cerrar Sesión
                                </a>
                            </li>
                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- end navbar-collapse -->
        </div>
        <!-- end container -->
    </div>
    <!-- end #header -->

    <!-- begin #home -->
    <div id="home" class="content has-bg home p-t-40" style="height: 657px;">
        <!-- begin content-bg -->
        <div class="content-bg">
            <img src="{{ URL::to('/../storage/app/home/'.$homes->id) }}" alt="Home">
        </div>
        <!-- end content-bg -->
        <!-- begin container -->
        <div class="container home-content">
            <h1 class="m-t-10">{{ $homes->title }}</h1>
            <p>{{ $homes->description }}</p>
            <br>
        </div>
        <!-- end container -->
    </div>
    <!-- end #home -->

    <!-- begin #contact -->
    <div id="contact" class="content" data-scrollview="true">
        <!-- begin container -->
        <div class="container fadeInDown contentAnimated finishAnimated" data-animation="true" data-animation-type="fadeInDown">
            <h2 class="content-title">Contacto</h2>
            <!-- begin row -->
            <div class="row">
                @include('contacts.fields')
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end #contact -->

    <!-- begin #branch_office -->
    <div id="branch_office" class="content" data-scrollview="true">
        <!-- begin container -->
        <div class="container fadeInDown contentAnimated finishAnimated" data-animation="true" data-animation-type="fadeInDown">
            <h2 class="content-title">Sucursales</h2>
            <p class="content-desc"></p>
            <!-- begin row -->
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-sm-12 col-lg-5">
                            <!-- List group -->
                            <ul class="list-group" id="contentMarkers">
                                <li class="list-group-item">Cras justo odio</li>
                                <li class="list-group-item">Dapibus ac facilisis in</li>
                                <li class="list-group-item">Morbi leo risus</li>
                            </ul>
                        </div>
                        <div class="col-sm-12 col-lg-7">
                            <div id="mapIndex" style="height: 350px !important" class="height-md width-full">{{-- content map --}}</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end #branch_office -->

    <a id="page" data-url="{{ URL::to('/') }}"></a>


    <!-- begin #footer -->
    <div id="footer" class="footer">
        <div class="container">
            <div class="footer-brand">
                <div class="footer-brand-logo"></div>
                Color Admin
            </div>
            <p>
                © Copyright Color Admin 2014 <br>
                An admin &amp; front end theme with serious impact. Created by <a href="#">SeanTheme</a>
            </p>
            <p class="social-list">
                <a href="#"><i class="fa fa-facebook fa-fw"></i></a>
                <a href="#"><i class="fa fa-instagram fa-fw"></i></a>
                <a href="#"><i class="fa fa-twitter fa-fw"></i></a>
                <a href="#"><i class="fa fa-google-plus fa-fw"></i></a>
                <a href="#"><i class="fa fa-dribbble fa-fw"></i></a>
            </p>
        </div>
    </div>
    <!-- end #footer -->

    <!-- begin theme-panel -->
    <div class="theme-panel">
        <a href="javascript:;" data-click="theme-panel-expand" class="theme-collapse-btn"><i class="fa fa-cog"></i></a>
        <div class="theme-panel-content">
            <ul class="theme-list clearfix">
                <li><a href="javascript:;" class="bg-purple" data-theme="purple" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Purple" data-original-title="" title="">&nbsp;</a></li>
                <li><a href="javascript:;" class="bg-blue" data-theme="blue" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Blue" data-original-title="" title="">&nbsp;</a></li>
                <li class="active"><a href="javascript:;" class="bg-green" data-theme="default" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Default" data-original-title="" title="">&nbsp;</a></li>
                <li><a href="javascript:;" class="bg-orange" data-theme="orange" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Orange" data-original-title="" title="">&nbsp;</a></li>
                <li><a href="javascript:;" class="bg-red" data-theme="red" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Red" data-original-title="" title="">&nbsp;</a></li>
            </ul>
        </div>
    </div>
    <!-- end theme-panel -->
</div>
<!-- end #page-container -->

<!-- ================== BEGIN BASE JS ================== -->
{!! Html::script("assets/plugins/jquery/jquery-1.9.1.min.js") !!}
{!! Html::script("assets/plugins/jquery/jquery-migrate-1.1.0.min.js") !!}
{!! Html::script("assets/plugins/bootstrap/js/bootstrap.min.js") !!}
<!--[if lt IE 9]>
{!! Html::script("assets/crossbrowserjs/html5shiv.js") !!}
{!! Html::script("assets/crossbrowserjs/respond.min.js") !!}
{!! Html::script("assets/crossbrowserjs/excanvas.min.js") !!}
<![endif]-->
{!! Html::script("assets/plugins/jquery-cookie/jquery.cookie.js") !!}
{!! Html::script("assets/plugins/scrollMonitor/scrollMonitor.js") !!}
{!! Html::script("assets/js/apps.min.js") !!}
{!! Html::script("js/app/app.js") !!}
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PLUGINS================== -->
{!!Html::script('https://maps.googleapis.com/maps/api/js?key=AIzaSyAtBGvgzOl-tZMYru-Kt74mlLK1OSS-370&libraries=places', array('defer' => 'defer'))!!}
{!! Html::script('assets/plugins/parsley/dist/parsley.js') !!}
{!! Html::script('assets/plugins/kartik-v-bootstrap-fileinput/js/fileinput.js') !!}
{!! Html::script('assets/plugins/kartik-v-bootstrap-fileinput/js/locales/es.js') !!}
{!! Html::script('assets/plugins/bootstrap-toastr/toastr.min.js') !!}
{!! Html::script('assets/plugins/blockUI/blockui.min.js') !!}
<!-- ================== END PLUGINS ================== -->


{!! Html::script("js/utils.js") !!}

<script>
    $(document).ready(function() {
        App.init();
    });
</script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','../../../../www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-53034621-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>