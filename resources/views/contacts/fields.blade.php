<div class="col-lg-offset-3 col-lg-6 col-sm-12 p-t-5 text-center loading-contact">
    <div class="panel panel-default">
        <div class="panel-body">
            {!! Form::open(['id' => 'formContact', 'name' => 'formValidateContact', 'data-parsley-validate' => 'true','data-action'=>'']) !!}
            {!! Form::hidden('_method', null) !!}
            <!-- Name Field -->
                <div class="form-group col-sm-12 col-lg-4">
                    {!! Form::label('name', 'NOMBRE') !!}
                    {!! Form::text('name', null, ['class' => 'form-control','data-parsley-required' => 'true',
                        'data-parsley-required-message' => 'Ingrese un nombre.',
                        'data-parsley-errors-container' => '#name_error']) !!}
                    <div id="name_error"></div>
                </div>

                <!-- Email Field -->
                <div class="form-group col-sm-12 col-lg-4">
                    {!! Form::label('email', 'EMAIL') !!}
                    {!! Form::email('email', null, ['class' => 'form-control','data-parsley-required' => 'true',
                        'data-parsley-required-message' => 'Ingrese un Email.',
                        'data-parsley-type' => 'email',
                        'data-parsley-type-message' => 'Formato de Email incorrecto.',
                        'data-parsley-errors-container' => '#email_error']) !!}
                    <div id="email_error"></div>
                </div>

                <!-- Subject Field -->
                <div class="form-group col-sm-12 col-lg-4">
                    {!! Form::label('subject', 'ASUNTO') !!}
                    {!! Form::text('subject', null, ['class' => 'form-control','data-parsley-required' => 'true',
                        'data-parsley-required-message' => 'Ingrese un asunto.',
                        'data-parsley-errors-container' => '#subject_error']) !!}
                    <div id="subject_error"></div>
                </div>

                <!-- Description Field -->
                <div class="form-group col-sm-12 col-lg-12 p-b-5">
                    {!! Form::label('description', 'DESCRIPCION') !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control', 'rows'=>'3','data-parsley-required' => 'true',
                        'data-parsley-required-message' => 'Ingrese una descripcion.',
                        'data-parsley-errors-container' => '#description_error']) !!}
                    <div id="description_error"></div>
                </div>

                <!-- Submit Field -->
                <div class="form-group col-sm-12 text-center p-t-10">
                    <a href="javascript:;" class="btn btn-primary" id="btnSendMail"><i class="fa fa-envelope-open-o"></i> Enviar</a>
                </div>
                {!! Form::close() !!}
        </div>
    </div>
</div>

