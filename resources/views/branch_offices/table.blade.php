<table class="table table-responsive" id="branchOffices-table">
    <thead>
        <tr>
            <th>Name</th>
        <th>Address</th>
        <th>Lat</th>
        <th>Lng</th>
        <th>Description</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($branchOffices as $branchOffice)
        <tr>
            <td>{!! $branchOffice->name !!}</td>
            <td>{!! $branchOffice->address !!}</td>
            <td>{!! $branchOffice->lat !!}</td>
            <td>{!! $branchOffice->lng !!}</td>
            <td>{!! $branchOffice->description !!}</td>
            <td>
                {!! Form::open(['route' => ['branchOffices.destroy', $branchOffice->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('branchOffices.show', [$branchOffice->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('branchOffices.edit', [$branchOffice->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>