{!! Form::open(['id' => 'formBranchOffice', 'name' => 'formValidateBranchOffice', 'data-parsley-validate' => 'true','data-action'=>'']) !!}
{!! Form::hidden('_method', null, ['data-id' => '1']) !!}
<div class="modal fade in" id="modal_home" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="close_modal_branch_office"><i class="fa fa-times"></i></button>
                <h4 class="modal-title">Configuracion de Inicio</h4>
            </div>
            <div class="modal-body loading-modal-home">
                <div class="row validate-home">
                    @include('homes.fields')
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}