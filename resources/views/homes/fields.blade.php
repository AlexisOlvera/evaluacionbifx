<!-- Title Field -->
<div class="form-group col-sm-12">
    {!! Form::label('title', 'Titulo Principal:') !!}
    {!! Form::text('home[title]', null, ['id' => 'inputTitle', 'class' => 'form-control','data-parsley-required' => 'true',
        'data-parsley-required-message' => 'Ingrese un titulo.',
        'data-parsley-errors-container' => '#title_error']) !!}
    <div id="title_error"></div>
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Descripcion:') !!}
    {!! Form::textarea('home[description]', null, ['id' => 'inputDescription', 'class' => 'form-control', 'rows'=>'3', 'data-parsley-required' => 'true',
        'data-parsley-required-message' => 'Ingrese una descripcion.',
        'data-parsley-errors-container' => '#description_error']) !!}
    <div id="description_error"></div>
</div>

<!-- Image Field -->
{{--<div class="form-group col-sm-6">
    {!! Form::label('image', 'Seleccionar Imagen:') !!}
    {!! Form::text('image', null, ['class' => 'form-control']) !!}
</div>--}}

<div class="col-sm-12 col-lg-12 p-b-20">
    <!-- Url Image Field -->
    <input id="input_image" type="file" class="file"
           data-preview-file-type="text"
           data-show-remove="false"
           data-show-upload="false"
           accept="image/*"
           data-parsley-required-message = "Debe adjuntar una imagen."
           data-parsley-errors-container = "#image_error">
    {!! Form::hidden('home[image]', null, ['id' => 'image']) !!}
    {!! Form::hidden('home[imageFormat]', null, ['id' => 'imageFormat']) !!}
    <div id="image_error"></div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    <a href="javascript:;" class="btn btn-primary" id="btnSaveHome">Guardar</a>
    <a data-dismiss="modal" class="btn btn-default">Cancelar</a>
</div>
