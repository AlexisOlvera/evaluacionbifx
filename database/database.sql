-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.19 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para test_laravel_5.4
CREATE DATABASE IF NOT EXISTS `test_laravel_5.4` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `test_laravel_5.4`;

-- Volcando estructura para tabla test_laravel_5.4.about
CREATE TABLE IF NOT EXISTS `about` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla test_laravel_5.4.about: ~0 rows (aproximadamente)
DELETE FROM `about`;
/*!40000 ALTER TABLE `about` DISABLE KEYS */;
/*!40000 ALTER TABLE `about` ENABLE KEYS */;

-- Volcando estructura para tabla test_laravel_5.4.branch_office
CREATE TABLE IF NOT EXISTS `branch_office` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla test_laravel_5.4.branch_office: ~3 rows (aproximadamente)
DELETE FROM `branch_office`;
/*!40000 ALTER TABLE `branch_office` DISABLE KEYS */;
INSERT INTO `branch_office` (`id`, `name`, `address`, `lat`, `lng`, `description`) VALUES
	(1, 'GERARDO ANDRES CABRIALES OROZCO', '\r\nCalz. de los Arcos 173-A\r\nLos Arcos\r\n76024 Santiago de Querétaro, Qro. ', 20.5993463, -100.3736013, 'Sucursal 1'),
	(2, 'TECNOLOGIA ELECTRONEUMATICA Y CONTROL', 'Luis Pasteur S/n, Centro, 76000 Santiago de Querétaro, Qro.', 20.593136, -100.3971291, 'Sucursal 2'),
	(3, 'TODO DE FILTROS  EMPAQUES Y SERVICIOS', 'Calle Paseo de las Artes 1531-B, Josefa Vergara, 76090 Santiago de Querétaro, Qro.', 20.586634, -100.3749101, 'Sucursal 3');
/*!40000 ALTER TABLE `branch_office` ENABLE KEYS */;

-- Volcando estructura para tabla test_laravel_5.4.contact
CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla test_laravel_5.4.contact: ~4 rows (aproximadamente)
DELETE FROM `contact`;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` (`id`, `name`, `email`, `subject`, `description`) VALUES
	(3, 'Alexis OT', 'demo@mail.com', 'venta', 'prueba de descripcion de ventas'),
	(4, 'Alexis OT', 'demo@mail.com', 'Venta', 'Solicito informacion acerca de sus productos'),
	(5, 'Aldaawda', 'dawd@dawd.com', 'dawdaw', 'awdaw adawwdawdaw ad awdad'),
	(6, 'dawdaw', 'demo@mail.com', 'adawdawd', 'dawdaw adawd aw da');
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;

-- Volcando estructura para tabla test_laravel_5.4.home
CREATE TABLE IF NOT EXISTS `home` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla test_laravel_5.4.home: ~1 rows (aproximadamente)
DELETE FROM `home`;
/*!40000 ALTER TABLE `home` DISABLE KEYS */;
INSERT INTO `home` (`id`, `title`, `description`, `image`) VALUES
	(1, 'TITULO PRINCIPAL DE PRUEBA', 'ESTO ES UNA DESCRIPCION DE PRUEBA', '1.jpg');
/*!40000 ALTER TABLE `home` ENABLE KEYS */;

-- Volcando estructura para tabla test_laravel_5.4.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla test_laravel_5.4.users: ~1 rows (aproximadamente)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Alexis Olvera', 'demo@mail.com', '$2y$10$VH4.kxNBba17LVVAOy/lJO/miIkL5.FlH2NfFtX3iod1RikIX143y', 'nispo8ePBqau3PrwDyh8QSQpTpn2OcKzYggj8yqiE1vLayHhjzSvR4DqN32U', '2018-10-18 13:25:43', '2018-10-18 13:25:43');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
