//Ajax para archivos adjuntos
$.ajaxFormData = function ($params) {
    var formData = ($params.form !== null) ? new FormData($('#' + $params.form)[0]) : null;
    //var formData = new FormData($('#' + $params.form)[0]);

    $.ajax({
        type: $params.type,
        url: $params.url,
        contentType: false,
        processData: false,
        cache: false,
        headers: {'X-CSRF-TOKEN': $('[name="csrf_token"]').attr('content')},
        beforeSend: function () {
            $.setLoading($params.loadingSelector, "Espere un momento...");
        },
        data: formData,
        success: function (data) {
            $($params.loadingSelector).unblock();

            if (data.message)
                toastr.success(data.message, $params.crud);

            $params.successCallback(data);
        },
        error: function (error) {
            var data = JSON.parse(error.responseText);

            if (data.errors) {
                $.each(data.errors, function () {
                    toastr.error(this, $params.crud, {
                        closeButton: true,
                        timeOut: 6000,
                        progressBar: true,
                        allowHtml: true
                    });
                });
            } else {
                toastr.error(data.message, $params.crud, {
                    closeButton: true,
                    timeOut: 4000,
                    progressBar: true,
                    allowHtml: true
                });
            }

            $($params.loadingSelector).unblock();
            $params.errorCallback(error);
        }
    });
};

$.setLoading = function ($selector, $text) {
    $($selector).block({
        message: $text,
        centerY: true,
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });
};