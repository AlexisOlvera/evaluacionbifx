$(document).ready(function () {
    //resets
    $('#formContact').trigger("reset");

    var inputImage = document.getElementById('input_image');
    $('#input_image').on('change', readFile);


    /************************************* BEGIN HOME ******************************************/
    /*
     * Open Modal Home
     */
    $(document).on("click", "#btnConfigHome", function () {
        $("#modal_home").modal("show");
        $('#input_image').attr('required', 'true');
        $('#inputTitle').val('');
        $('#inputDescription').val('');
    });
    /*
     * Btn Save Home
     */
    $(document).on("click", "#btnSaveHome", function () {
        $('input[name=_method]').val('PATCH');

        if (true === $('form[name="formValidateHome"]').parsley().validate()) {
            var params = {
                type: 'POST',
                url: 'homes/' + $('input[name=_method]').data('id'),
                loadingSelector: $(".loading-modal-home"),
                form: 'formHome',
                crud: '<i class="fa fa-server"></i> Server Response',
                successCallback: function (data) {
                    $("#modal_home").modal("hide");
                    location.reload();
                },
                errorCallback: function (error) {
                    console.log('¡Error!', error);
                    $("#modal_home").modal("hide");
                }
            };
            $.ajaxFormData(params);
        } else {
            toastr.error('Tiene que llenar todos los campos.', 'Error!', {
                closeButton: true,
                timeOut: 6000,
                progressBar: true,
                allowHtml: true
            });
        }
    });
    /************************************* END HOME ******************************************/


    /************************************* BEGIN CONTACT ******************************************/
    /*
     * Btn Save Home
     */
    $(document).on("click", "#btnSendMail", function () {
        $('input[name=_method]').val('POST');

        if (true === $('form[name="formValidateContact"]').parsley().validate()) {
            var params = {
                type: 'POST',
                url: 'contacts',
                loadingSelector: $(".loading-contact"),
                form: 'formContact',
                crud: '<i class="fa fa-server"></i> Server Response',
                successCallback: function (data) {
                    console.log('¡Success!', data);
                    $('#formContact').trigger("reset");
                },
                errorCallback: function (error) {
                    console.log('¡Error!', error);
                }
            };
            $.ajaxFormData(params);
        } else {
            toastr.error('Tiene que llenar todos los campos.', 'Error!', {
                closeButton: true,
                timeOut: 6000,
                progressBar: true,
                allowHtml: true
            });
        }
    });
    /************************************* END CONTACT ******************************************/

    /************************************* BEGIN BRANCH OFFICE ******************************************/
    map = new google.maps.Map(document.getElementById('mapIndex'), {
        center: new google.maps.LatLng(20.5974327, -100.3540983),
        zoom: 13,
        styles:[{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}],
        mapTypeControl: false
    });

    $.ajax({
        type: 'GET',
        url: $('#page').data('url')+'/getBranchOffice',
        headers: {'_method': 'GET'},
        contentType: false,
        processData: false,
        cache: false,
        success: function (data) {

            var content = '',
                marker = '';

            if (data.success === 1) {
                $.each(data.data, function(index, value) {
                    console.log(value.name);
                    content += '<li class="list-group-item">\
                            <p>'+value.description+'<p>\
                            <p>'+value.name+'<p>\
                            <p>'+value.address+'<p>\
                        </li>';

                    marker = new google.maps.Marker({
                        position: {lat: value.lat, lng: value.lng},
                        map: map,
                        title: value.address
                    });

                });

                $('#contentMarkers').html(content);
            } else{
                $.showerrors(data.responseJSON, data.msg);
            }
        }, error: function (data) {
            toastr.error('Ocurrio un error al cargar las sucursales.', 'Error!', {
                closeButton: true,
                timeOut: 6000,
                progressBar: true,
                allowHtml: true
            });
        }
    });
    /************************************* END BRANCH OFFICE ******************************************/


    /*
     * Functions
     */
    function readFile()
    {
        var extension = $('#input_image').val().split('.').pop();
        document.getElementById('imageFormat').value = extension;

        if (this.files && this.files[0]) {
            var FR= new FileReader();
            FR.addEventListener("load", function(e) {
                document.getElementById('image').value = e.target.result;
                //$('#imagePreviewContainer').remove();
            });
            FR.readAsDataURL( this.files[0] );
        }
    }
});