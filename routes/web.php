<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});


Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('homes', 'homeController');

Route::resource('contacts', 'contactController');

Route::post('/send', 'EmailController@send');

Route::get('/getBranchOffice', 'branch_officeController@getBranchOffice');
Route::resource('branchOffices', 'branch_officeController');